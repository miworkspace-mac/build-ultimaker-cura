#!/bin/bash
#NEWLOC=`curl -L "https://ultimaker.com/software/ultimaker-cura" -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1 Safari/605.1.15' 2>/dev/null | grep -o https.*-Darwin.dmg | tail -1 | awk -F'"https:' '{print $NF}' | sed 's@u002F@@g' | tr '\\' '/'`
#if [ "x${NEWLOC}" != "x" ]; then
#	echo "https:${NEWLOC}"
#fi

#NEWLOC=`curl -L "https://github.com/Ultimaker/Cura/releases"  2>/dev/null | /usr/local/bin/htmlq -a href a | grep .dmg | head -1`


#if [ "x${NEWLOC}" != "x" ]; then
#	echo https://github.com"${NEWLOC}"
#fi


NEWLOC=`curl "https://api.github.com/repos/Ultimaker/Cura/releases/latest" | grep .dmg| tail -1 | awk -F'"' '{print $4}'`


if [ "x${NEWLOC}" != "x" ]; then
	echo ${NEWLOC}
fi